# README
This code is an implementation of the extremal region extraction algorithm described in 
Linear Time Maximally Stable Extremal Regions
David Nistér, Henrik Stewénius

This implementation has beed developed and used as part of the work described in 
Fast and portable text detection, F. Odone L. Zini
Submitted to Machine Vision and Applications

The code requires a C++0x compiler.

The core of this library is the class ERExtractor, that extracts the Extremal Regions from a gray scale image. 
The extraction is done in two steps: in a first pass the algorithm extracts all the "dark" regions, in the second pass it extracts all the bright regions (for more details Linear Time Maximally Stable Extremal Regions).
The two passes can be called separately to process the two sets of regions separately (dark and bright), but the need to be called in this specific order. 

The library includes a set of containers used by the er extraction algorithm. Most of them are simple implementation of containers that are already available in the c++ standard library. We used them to standardize behaviour of some operation (e.g. std::vector clear) across different compilers.

# License #

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.