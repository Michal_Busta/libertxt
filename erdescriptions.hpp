/*    
    This implementation has beed developed and used as part of the work described in 
    Fast and portable text detection, F. Odone L. Zini
    Submitted to Machine Vision and Application
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef EXTREMALREGIONDESCRIPTIONS_HPP
#define EXTREMALREGIONDESCRIPTIONS_HPP

class NONE{
public:
    void add(int x, int y) const {

    }

    void merge(const NONE& d) const {

    }

};

class GeometricalMoments{
public:
    GeometricalMoments(){
        memset(moments, 0, 9);
    }

    void add(int x, int y)  {
        const int x2 = x*x;
        const int y2 = y*y;
        moments[0] += x;
        moments[1] += y;
        moments[2] += x2;
        moments[3] += x * y;
        moments[4] += y2;
        moments[5] += x2 * y;
        moments[6] += x * y2;
        moments[7] += x2 * x;
        moments[8] += y2 * y;
    }

    void merge(const GeometricalMoments& d)  {
        moments[0] += d.moments[0];
        moments[1] += d.moments[1];
        moments[2] += d.moments[2];
        moments[3] += d.moments[3];
        moments[4] += d.moments[4];
        moments[5] += d.moments[5];
        moments[6] += d.moments[6];
        moments[7] += d.moments[7];
        moments[8] += d.moments[8];
    }

private:
    double moments[9];
};


#endif // EXTREMALREGIONDESCRIPTIONS_HPP
