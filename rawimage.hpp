/*    
    This implementation has beed developed and used as part of the work described in 
    Fast and portable text detection, F. Odone L. Zini
    Submitted to Machine Vision and Application
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RAWIMAGE_HPP
#define RAWIMAGE_HPP

template <class T>
class RawImageWrapper{
public:
    RawImageWrapper(): width(0), height(0), data(0), own(false) {}
    RawImageWrapper(int width, int height, unsigned char* data): width(width), height(height), data(data), own(false) {}
    RawImageWrapper(int width, int height) : width(width), height(height), own(true) {
        data = new unsigned char[width * height]();

    }

    RawImageWrapper<T>& operator =(const RawImageWrapper<T>& o){
        width = o.width;
        height = o.height;
        own = true;
        data = new unsigned char[width * height]();
        memcpy(data, o.data, width*height*sizeof(T));
        return *this;
    }

    T& operator()(const int i, const int j){
        return data[i * width + j];
    }

    ~RawImageWrapper(){
        if(own){
            delete[] data;
        }
    }

    void clear(){
        memset(data, width*height*sizeof(T), 0);
    }

    unsigned char* data;
    int width;
    int height;
    bool own;
};


#endif // RAWIMAGE_HPP
