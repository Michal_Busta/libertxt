/*
    This code is an implementation of the algorithm described in 
    Linear Time Maximally Stable Extremal Regions
    David Nistér, Henrik Stewénius

    This implementation has beed developed and used as part of the work described in 
    Fast and portable text detection, F. Odone L. Zini
    Submitted to Machine Vision and Application
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ER_EXTRACTOR_H_
#define _ER_EXTRACTOR_H_

#include <opencv2/opencv.hpp>
#include <string.h>
#include <list>
#include <vector>
#include <stack>
#include <math.h>
#include <stdlib.h>
#include <algorithm>
#include "containers.hpp"
#include "rawimage.hpp"


template <class INCREMENTAL_DESCRIPTOR>
class ExtremalRegion{
public:
    ExtremalRegion(){}
    ExtremalRegion(int level, int pixel): level(level), pixel(pixel), parent(0), childs(0), siblings(0){
        minx = miny = 99999;
        maxx = maxy = 0;
        m00 = 0;
        hasbb = false;
    }
    mutable bool hasbb;
    mutable long bbareav;
    mutable cv::Rect bb;
    INCREMENTAL_DESCRIPTOR descr;

    
    void add(int xi, int yi){
        m00++;
        minx = std::min(minx, xi);
        miny = std::min(miny, yi);
        maxx = std::max(maxx, xi);
        maxy = std::max(maxy, yi);
        descr.add(xi, yi);
    }

    void merge( ExtremalRegion<INCREMENTAL_DESCRIPTOR>* child){
        m00 += child->m00;
        minx = std::min(minx, child->minx);
        miny = std::min(miny, child->miny);
        maxx = std::max(maxx, child->maxx);
        maxy = std::max(maxy, child->maxy);
        descr.merge(child->descr);
        child->siblings = childs;
        childs = child;
        child->parent = this;
    }

    long bbarea() const{
        if(!hasbb){
            bb =getBB();
            bbareav = bb.area();
        }
        return bbareav;
    }
    
    const cv::Rect &getBB() const {
        if(!hasbb){
            hasbb = true;
            bb = cv::Rect(minx, miny, maxx-minx, maxy-miny);
        }
        return bb;
    }


    int m00;
    int minx, maxx, miny, maxy;
    int level;
    int pixel;
    ExtremalRegion<INCREMENTAL_DESCRIPTOR>* parent;
    ExtremalRegion<INCREMENTAL_DESCRIPTOR>* childs;
    ExtremalRegion<INCREMENTAL_DESCRIPTOR>* siblings;
    INCREMENTAL_DESCRIPTOR d;
};


class BoundaryQueuePixel{
public:
    BoundaryQueuePixel( int image_pixel, int edge): image_pixel(image_pixel), edge(edge){}

    int image_pixel;
    int edge;
};

template <class INCREMENTAL_DESCRIPTOR>
class ERExtractor {
public:
    ERExtractor(int width, int height) :accessible_pixels(width, height),cocosPool(100000), cocos(100000){
        clearMask();
        image_neighborhood[0] = -1;
        image_neighborhood[1] = +1;
        image_neighborhood[2] = width - 1;
        image_neighborhood[3] = width;
        image_neighborhood[4] = width + 1;
        image_neighborhood[5] = - width - 1;
        image_neighborhood[6] = - width;
        image_neighborhood[7] = - width + 1;
    }

    void clear(){
        cocosPool.clear();
        cocos.clear();
        for(unsigned i = 0; i < 256; i++){
            boundaryPixels[i].clear();
        }
    }

    template <class FILTER>
    void firstPass(const RawImageWrapper<unsigned char>& imm, const FILTER&  filt, std::list<ExtremalRegion<INCREMENTAL_DESCRIPTOR> >& ris){
        this->operator() <ERFirstPassPolicy>(imm, filt, ris);
    }

    template <class FILTER>
    void secondPass(const RawImageWrapper<unsigned char>& imm, const FILTER&  filt, std::list<ExtremalRegion<INCREMENTAL_DESCRIPTOR> >& ris){
        this->operator() <ERSecondPassPolicy>(imm, filt, ris);
    }


private:
    class ERFirstPassPolicy{
    public:
        static inline bool lower(const int& a, const int& b)  {
            return a < b;
        }

        static  int nextIndex(const StaticSizeVector<BoundaryQueuePixel> boundaryPixels[], int startIndex = 0){
            int priority_queue_index = startIndex;
            for(; priority_queue_index < GRAY_LEVELS && boundaryPixels[priority_queue_index].empty(); priority_queue_index++){}
            if(priority_queue_index != GRAY_LEVELS){
                return priority_queue_index;
            }else{
                return -1;
            }
        }

        static  bool isSecond() {
            return false;
        }

        static const int GRAY_LEVELS = 256;
        static const int ACCESSIBLE = 1;
        static const int NOT_ACCESSIBLE = 0;
        static const int BORDER = 2;
        static const int DUMMY_LEVEL = GRAY_LEVELS;
    };

    class ERSecondPassPolicy{
    public:
        inline static bool lower(const int& a, const int& b)  {
            return a > b;
        }

        static  bool isSecond() {
            return true;
        }

        static  int nextIndex(const StaticSizeVector<BoundaryQueuePixel> boundaryPixels[], int startIndex = 255){
            int priority_queue_index = startIndex;
            for(; priority_queue_index >= 0 && boundaryPixels[priority_queue_index].empty(); priority_queue_index--){}
            return priority_queue_index;
        }

        static const int GRAY_LEVELS = 256;
        static const int ACCESSIBLE = 0;
        static const int NOT_ACCESSIBLE = 1;
        static const int BORDER = 2;
        static const int DUMMY_LEVEL = -1;
    };

    template <class PASS_POLICY, class FILTER>
    void operator()(const RawImageWrapper<unsigned char>& imm, const FILTER&  filt, std::list<ExtremalRegion<INCREMENTAL_DESCRIPTOR> >& ris){
        cocos.clear();
        for(StaticSizeVector<BoundaryQueuePixel>* i = boundaryPixels, * end = boundaryPixels + PASS_POLICY::GRAY_LEVELS; i < end; i++){
            i->clear();
        }

        cocos.push_back( new (cocosPool.nextAddress()) ExtremalRegion<INCREMENTAL_DESCRIPTOR>(PASS_POLICY::DUMMY_LEVEL, 0));
        unsigned int image_pixel = imm.width + 1;
        unsigned int edge = 0;
        unsigned int currentLevel = imm.data[image_pixel];
        accessible_pixels.data[image_pixel] = PASS_POLICY::ACCESSIBLE;
step_3:
        cocos.push_back( new (cocosPool.nextAddress()) ExtremalRegion<INCREMENTAL_DESCRIPTOR>(currentLevel, image_pixel));
step_4:
        for(; edge < NEIGHBORHOOD_SIZE; edge++){
            const int new_image_pixel = image_pixel + image_neighborhood[edge];
            if(accessible_pixels.data[new_image_pixel] == PASS_POLICY::NOT_ACCESSIBLE){
                const unsigned int new_image_level = imm.data[new_image_pixel];
                accessible_pixels.data[new_image_pixel] = PASS_POLICY::ACCESSIBLE;
                if(!PASS_POLICY::lower(new_image_level, currentLevel)){
                    boundaryPixels[new_image_level].push_back(BoundaryQueuePixel(new_image_pixel, 0));
                }else{
                    boundaryPixels[currentLevel].push_back(BoundaryQueuePixel(image_pixel, edge + 1));
                    image_pixel = new_image_pixel;
                    edge = 0;
                    currentLevel = new_image_level;
                    goto step_3;
                }

            }
        }
        cocos.back()->add(image_pixel % imm.width, image_pixel / imm.width);
        int priority_queue_index = PASS_POLICY::nextIndex(boundaryPixels, currentLevel);
        if(priority_queue_index < 0){
            filterERs(cocos.back(), filt, ris);
            return;
        }
        const BoundaryQueuePixel& pop = boundaryPixels[priority_queue_index].back();
        image_pixel = pop.image_pixel;
        edge = pop.edge;
        boundaryPixels[priority_queue_index].pop_back();

        if(priority_queue_index != currentLevel){
            currentLevel = priority_queue_index;
            processStack<PASS_POLICY>(priority_queue_index, image_pixel);
        }
        goto step_4;
    }

    template <class PASS_POLICY>
    void processStack(int new_pixel_gray_level, int pixel){
        do{
            ExtremalRegion<INCREMENTAL_DESCRIPTOR>* top = cocos.back();
            cocos.pop_back();
            if(PASS_POLICY::lower(new_pixel_gray_level, cocos.back()->level)){
                cocos.push_back( new (cocosPool.nextAddress()) ExtremalRegion<INCREMENTAL_DESCRIPTOR>(new_pixel_gray_level, pixel));
                cocos.back()->merge(top);
                return;
            }
            cocos.back()->merge(top);
        }while(PASS_POLICY::lower(cocos.back()->level, new_pixel_gray_level));
    }

    void clearMask(){
        for(unsigned i = 0; i < accessible_pixels.width * accessible_pixels.height; i++){
            accessible_pixels.data[i] = ERFirstPassPolicy::NOT_ACCESSIBLE;
        }
        for(unsigned i = 0; i < accessible_pixels.width; i++){
            accessible_pixels.data[i] = ERFirstPassPolicy::BORDER;
            accessible_pixels.data[i + (accessible_pixels.height - 1)* accessible_pixels.width] = ERFirstPassPolicy::BORDER;
        }

        for(unsigned i = 0; i < accessible_pixels.height; i++){
            accessible_pixels.data[accessible_pixels.width*i] = ERFirstPassPolicy::BORDER;
            accessible_pixels.data[accessible_pixels.width*i + accessible_pixels.width - 1] = ERFirstPassPolicy::BORDER;
        }
    }
    
    
    template <class FILTER>
    void filterERs(ExtremalRegion<INCREMENTAL_DESCRIPTOR>* er, FILTER& filter, std::list<ExtremalRegion<INCREMENTAL_DESCRIPTOR> >& ris){
        if(filter.accept(*er)){
	  ris.push_back(*er);
        }
        for (ExtremalRegion<INCREMENTAL_DESCRIPTOR>* child = er->childs; child; child = child->siblings) {
	  filterERs(child, filter, ris);
        }
    }
    
    static const int NEIGHBORHOOD_SIZE = 8;
    RawImageWrapper<int> accessible_pixels;
    Pool<ExtremalRegion<INCREMENTAL_DESCRIPTOR> > cocosPool;
    ArrayListVector<ExtremalRegion<INCREMENTAL_DESCRIPTOR>*> cocos;
    StaticSizeVector<BoundaryQueuePixel> boundaryPixels[256];
    int image_neighborhood[NEIGHBORHOOD_SIZE];  
};

#endif /* _ER_EXTRACTOR_H_ */

